/* stocks-symbol-info.c
 *
 * Copyright 2023 Stephan Vedder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stocks-symbol-info.h"

#include "yahoo-finance/yahoo-finance-search-model.h"

struct _StocksSymbolInfo
{
  AdwBin parent_instance;

  StocksSymbol *symbol;
  GtkLabel *price;
  GtkLabel *change;
};

G_DEFINE_TYPE (StocksSymbolInfo, stocks_symbol_info, ADW_TYPE_BIN)

enum
{
  PROP_0,
  PROP_SYMBOL,
  N_PROPS
};
static GParamSpec *properties[N_PROPS];

StocksSymbolInfo *
stocks_symbol_info_new (StocksSymbol *symbol)
{
  return g_object_new (STOCKS_TYPE_SYMBOL_INFO,
                       "symbol", symbol,
                       NULL);
}

static void on_market_change_changed (GObject *symbol,
                                      GParamSpec *pspec,
                                      gpointer user_data);

static void
stocks_symbol_info_get_property (GObject *object,
                                 guint prop_id,
                                 GValue *value,
                                 GParamSpec *pspec)
{
  StocksSymbolInfo *self = STOCKS_SYMBOL_INFO (object);

  switch (prop_id)
    {
    case PROP_SYMBOL:
      g_value_set_object (value, self->symbol);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
stocks_symbol_info_set_property (GObject *object,
                                 guint prop_id,
                                 const GValue *value,
                                 GParamSpec *pspec)
{
  StocksSymbolInfo *self = STOCKS_SYMBOL_INFO (object);

  switch (prop_id)
    {
    case PROP_SYMBOL:
      g_set_object (&self->symbol, g_value_get_object (value));
      // This is set only once during construct - don't care about freeing it
      if (self->symbol)
        {
          on_market_change_changed (G_OBJECT (self->symbol), NULL, self);
          g_signal_connect_object (self->symbol, "notify::market-change", G_CALLBACK (on_market_change_changed), self, G_CONNECT_DEFAULT);
        }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static gboolean
double_to_percent (GBinding *binding,
                   const GValue *from,
                   GValue *to,
                   gpointer user_data)
{
  g_value_take_string (to, g_strdup_printf ("%.2f%%", g_value_get_double (from)));
  return TRUE;
}

static gboolean
double_to_currency (GBinding *binding,
                    const GValue *from,
                    GValue *to,
                    gpointer user_data)
{
  StocksSymbol *symbol = user_data;

  switch (stocks_symbol_get_currency (symbol))
    {
    case STOCKS_CURRENCY_AUD:
    case STOCKS_CURRENCY_CAD:
    case STOCKS_CURRENCY_MXN:
    case STOCKS_CURRENCY_USD:
      g_value_take_string (to, g_strdup_printf ("%.2f$", g_value_get_double (from)));
      break;
    case STOCKS_CURRENCY_EUR:
      g_value_take_string (to, g_strdup_printf ("%.2f€", g_value_get_double (from)));
      break;
    case STOCKS_CURRENCY_GBP:
      g_value_take_string (to, g_strdup_printf ("%.2f£", g_value_get_double (from)));
      break;
    default:
      g_value_take_string (to, g_strdup_printf ("%.2f?", g_value_get_double (from)));
      break;
    }

  return TRUE;
}

static void
stocks_symbol_info_map (GtkWidget *widget)
{
  StocksSymbolInfo *self = STOCKS_SYMBOL_INFO (widget);

  g_object_bind_property_full (self->symbol, "market-change",
                               self->change, "label",
                               G_BINDING_SYNC_CREATE,
                               double_to_percent,
                               NULL,
                               NULL,
                               NULL);

  g_object_bind_property_full (self->symbol, "price",
                               self->price, "label",
                               G_BINDING_SYNC_CREATE,
                               double_to_currency,
                               NULL,
                               self->symbol,
                               NULL);

  GTK_WIDGET_CLASS (stocks_symbol_info_parent_class)->map (widget);
}

static void
stocks_symbol_info_class_init (StocksSymbolInfoClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = stocks_symbol_info_get_property;
  object_class->set_property = stocks_symbol_info_set_property;

  widget_class->map = stocks_symbol_info_map;

  g_type_ensure (STOCKS_TYPE_SYMBOL);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/feliwir/stocks/stocks-symbol-info.ui");
  gtk_widget_class_bind_template_child (widget_class, StocksSymbolInfo, change);
  gtk_widget_class_bind_template_child (widget_class, StocksSymbolInfo, price);

  /**
   * StocksSymbolInfo:symbol:
   *
   * The symbol we want to show info for
   */
  properties[PROP_SYMBOL] =
      g_param_spec_object ("symbol", NULL, NULL,
                           STOCKS_TYPE_SYMBOL,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
on_market_change_changed (GObject *instance,
                          GParamSpec *pspec,
                          gpointer user_data)
{
  StocksSymbol *symbol = STOCKS_SYMBOL (instance);
  StocksSymbolInfo *self = user_data;

  gtk_widget_remove_css_class (GTK_WIDGET (self), "success");
  gtk_widget_remove_css_class (GTK_WIDGET (self), "error");

  if (stocks_symbol_get_market_change (symbol) >= 0.0)
    gtk_widget_add_css_class (GTK_WIDGET (self), "success");
  else
    gtk_widget_add_css_class (GTK_WIDGET (self), "error");
}

static void
stocks_symbol_info_init (StocksSymbolInfo *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
