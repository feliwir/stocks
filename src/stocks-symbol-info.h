/* stocks-symbol-info.h
 *
 * Copyright 2023 Stephan Vedder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "stocks-symbol.h"
#include <adwaita.h>

G_BEGIN_DECLS

#define STOCKS_TYPE_SYMBOL_INFO (stocks_symbol_info_get_type ())

G_DECLARE_FINAL_TYPE (StocksSymbolInfo, stocks_symbol_info, STOCKS, SYMBOL_INFO, AdwBin)
StocksSymbolInfo *stocks_symbol_info_new (StocksSymbol *symbol);

G_END_DECLS
