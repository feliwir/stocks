/* yahoo-finance-symbol-query.c
 *
 * Copyright 2023 Stephan Vedder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "yahoo-finance-symbol-query.h"
#include "../stocks-symbol.h"
#include "stocks.h"
#include "yahoo-finance-api.h"

#include <json-glib/json-glib.h>
#include <libsoup/soup.h>

struct _YahooFinanceSymbolQuery
{
  GObject parent_instance;

  SoupSession *session;
};

typedef struct
{
  GCallback callback;
  GListStore *symbols;
  gpointer user_data;
} YahooFinanceSymbolQueryCallbackData;

G_DEFINE_TYPE (YahooFinanceSymbolQuery, yahoo_finance_symbol_query, G_TYPE_OBJECT)

YahooFinanceSymbolQuery *
yahoo_finance_symbol_query_new ()
{
  return g_object_new (YAHOO_FINANCE_TYPE_SYMBOL_QUERY, NULL);
}

static void
yahoo_finance_symbol_query_class_init (YahooFinanceSymbolQueryClass *klass)
{
}

static void
yahoo_finance_symbol_query_init (YahooFinanceSymbolQuery *self)
{
  self->session = soup_session_new ();
}

static void
yahoo_finance_symbol_query_response_cb (GObject *source, GAsyncResult *result, gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (JsonParser) parser = NULL;
  g_autoptr (GBytes) bytes = NULL;
  GEnumClass *eclass;
  GEnumValue *ev;
  const char *ename;
  // Get the userdata
  YahooFinanceSymbolQueryCallbackData *data = user_data;
  GListStore *symbols = data->symbols;
  YahooFinanceSymbolQueryUpdateCb callback = (YahooFinanceSymbolQueryUpdateCb) data->callback;
  gpointer user_data2 = data->user_data;
  g_free (data);

  bytes = soup_session_send_and_read_finish (SOUP_SESSION (source), result, &error);

  // Usage here is the same as before
  if (error)
    {
      g_warning ("Unable to receive YahooFinance response: %s", error->message);
      return;
    }

  parser = json_parser_new ();
  json_parser_load_from_data (parser, g_bytes_get_data (bytes, NULL), g_bytes_get_size (bytes), &error);
  if (error)
    {
      g_warning ("Unable to parse YahooFinance response: %s", error->message);
      return;
    }

  g_autoptr (JsonReader) reader = json_reader_new (json_parser_get_root (parser));

  json_reader_read_member (reader, "quoteResponse");
  json_reader_read_member (reader, "result");

  guint symbols_count = json_reader_count_elements (reader);
  if (symbols_count != g_list_model_get_n_items (G_LIST_MODEL (symbols)))
    {
      g_warning ("Results count doesn't match input count");
      return;
    }

  for (guint idx = 0; idx < symbols_count; idx++)
    {
      StocksSymbol *symbol = g_list_model_get_item (G_LIST_MODEL (symbols), idx);
      json_reader_read_element (reader, idx);

      if (json_reader_read_member (reader, "regularMarketPrice"))
        stocks_symbol_set_price (symbol, json_reader_get_double_value (reader));
      json_reader_end_member (reader);

      if (json_reader_read_member (reader, "regularMarketChange"))
        stocks_symbol_set_market_change (symbol, json_reader_get_double_value (reader));
      json_reader_end_member (reader);

      if (json_reader_read_member (reader, "currency"))
        {
          ename = g_ascii_strdown (json_reader_get_string_value (reader), -1);
          eclass = g_type_class_ref (STOCKS_TYPE_CURRENCY);
          ev = g_enum_get_value_by_nick (eclass, ename);
          if (ev)
            g_object_set (symbol, "currency", ev->value, NULL);
          else
            g_warning ("Unknown currency: %s", ename);

          g_type_class_unref (eclass);
        }
      json_reader_end_member (reader);

      json_reader_end_element (reader);
    }

  json_reader_end_member (reader);
  json_reader_end_member (reader);

  callback (symbols, user_data2);
}

void
yahoo_finance_symbol_query_update_symbols (YahooFinanceSymbolQuery *self, GListStore *symbols, GCancellable *cancellable, GCallback callback, gpointer user_data)
{
  g_autoptr (GUri) uri = NULL;
  g_autoptr (GString) symbols_str = NULL;
  g_autoptr (GString) query = NULL;
  guint symbols_count;

  g_return_if_fail (YAHOO_FINANCE_IS_SYMBOL_QUERY (self));
  g_return_if_fail (symbols != NULL);

  symbols_str = g_string_new ("");
  symbols_count = g_list_model_get_n_items (G_LIST_MODEL (symbols));
  for (guint idx = 0; idx < symbols_count; idx++)
    {
      StocksSymbol *symbol = g_list_model_get_item (G_LIST_MODEL (symbols), idx);
      g_string_append (symbols_str, stocks_symbol_get_symbol (symbol));
      if (idx != (symbols_count - 1))
        g_string_append (symbols_str, ",");
    }

  query = g_string_new ("fields=regularMarketPrice,regularMarketChange,currency&symbols={id}");
  g_string_replace (query, "{id}", symbols_str->str, 0);

  uri = g_uri_build (G_URI_FLAGS_NONE, "https", NULL, YAHOO_HOST, -1, QUOTE_ROUTE, query->str, NULL);
  SoupMessage *msg = soup_message_new_from_uri (SOUP_METHOD_GET, uri);

  YahooFinanceSymbolQueryCallbackData *data = g_malloc (sizeof (YahooFinanceSymbolQueryCallbackData));
  data->callback = callback;
  data->user_data = user_data;
  data->symbols = symbols;

  soup_session_send_and_read_async (
      self->session,
      msg,
      G_PRIORITY_DEFAULT,
      cancellable,
      yahoo_finance_symbol_query_response_cb,
      data);
}
