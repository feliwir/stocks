/* yahoo-finance-search-model.c
 *
 * Copyright 2023 Stephan Vedder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "yahoo-finance-search-model.h"
#include "../stocks-symbol.h"
#include "yahoo-finance-api.h"
#include "yahoo-finance-symbol-query.h"

#include <json-glib/json-glib.h>
#include <libsoup/soup.h>

struct _YahooFinanceSearchModel
{
  GObject parent_instance;

  gchar *search_text;
  SoupSession *session;
  GListStore *symbol_list;
  YahooFinanceSymbolQuery *symbol_query;
  GCancellable *cancellable;
};

static GType
yahoo_finance_search_model_get_item_type (GListModel *list)
{
  YahooFinanceSearchModel *self = YAHOO_FINANCE_SEARCH_MODEL (list);
  return g_list_model_get_item_type (G_LIST_MODEL (self->symbol_list));
}

static guint
yahoo_finance_search_model_get_n_items (GListModel *list)
{
  YahooFinanceSearchModel *self = YAHOO_FINANCE_SEARCH_MODEL (list);
  g_return_val_if_fail (G_IS_LIST_MODEL (self->symbol_list), 0);
  return g_list_model_get_n_items (G_LIST_MODEL (self->symbol_list));
}

static gpointer
yahoo_finance_search_model_get_item (GListModel *list,
                                     guint position)
{
  YahooFinanceSearchModel *self = YAHOO_FINANCE_SEARCH_MODEL (list);
  g_return_val_if_fail (G_IS_LIST_MODEL (self->symbol_list), NULL);
  return g_list_model_get_item (G_LIST_MODEL (self->symbol_list), position);
}

static void
yahoo_finance_search_model_model_init (GListModelInterface *iface)
{
  iface->get_item_type = yahoo_finance_search_model_get_item_type;
  iface->get_n_items = yahoo_finance_search_model_get_n_items;
  iface->get_item = yahoo_finance_search_model_get_item;
}

G_DEFINE_TYPE_WITH_CODE (YahooFinanceSearchModel, yahoo_finance_search_model, G_TYPE_OBJECT, G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, yahoo_finance_search_model_model_init))

enum
{
  PROP_0,
  PROP_SEARCH_TEXT,
  N_PROPS
};
static GParamSpec *properties[N_PROPS];

static void
yahoo_finance_search_model_query_update_search (YahooFinanceSearchModel *self);

static void
yahoo_finance_search_model_finalize (GObject *object)
{
  YahooFinanceSearchModel *symbol_list = YAHOO_FINANCE_SEARCH_MODEL (object);

  g_object_unref (symbol_list->session);

  G_OBJECT_CLASS (yahoo_finance_search_model_parent_class)->finalize (object);
}

static void
yahoo_finance_search_model_get_property (GObject *object,
                                         guint prop_id,
                                         GValue *value,
                                         GParamSpec *pspec)
{
  YahooFinanceSearchModel *self = YAHOO_FINANCE_SEARCH_MODEL (object);

  switch (prop_id)
    {
    case PROP_SEARCH_TEXT:
      g_value_set_string (value, self->search_text);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
yahoo_finance_search_model_set_property (GObject *object,
                                         guint prop_id,
                                         const GValue *value,
                                         GParamSpec *pspec)
{
  YahooFinanceSearchModel *self = YAHOO_FINANCE_SEARCH_MODEL (object);

  switch (prop_id)
    {
    case PROP_SEARCH_TEXT:
      self->search_text = g_strdup (g_value_get_string (value));
      yahoo_finance_search_model_query_update_search (self);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
yahoo_finance_search_model_class_init (YahooFinanceSearchModelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = yahoo_finance_search_model_get_property;
  object_class->set_property = yahoo_finance_search_model_set_property;
  object_class->finalize = yahoo_finance_search_model_finalize;

  /**
   * YahooFinanceSearchModel:search_text:
   *
   * The current search text of our model
   */
  properties[PROP_SEARCH_TEXT] =
      g_param_spec_string ("search_text", "Search text", "The text we're searching for",
                           NULL,
                           G_PARAM_READWRITE);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static gboolean
yahoo_finance_search_model_update (gpointer user_data)
{
  YahooFinanceSearchModel *self = user_data;
  g_debug ("Updating symbols");
  yahoo_finance_search_model_query_update_symbols (self, self->symbol_list);
  return G_SOURCE_CONTINUE;
}

static void
yahoo_finance_search_model_init (YahooFinanceSearchModel *self)
{
  self->session = soup_session_new ();
  self->symbol_query = yahoo_finance_symbol_query_new ();

  g_timeout_add (2000, yahoo_finance_search_model_update, self);
}
static void
yahoo_finance_search_model_update_cb (GListStore *symbols, gpointer user_data)
{
  YahooFinanceSearchModel *self = user_data;
  guint oldSize = g_list_model_get_n_items (G_LIST_MODEL (self->symbol_list));
  self->symbol_list = symbols;
  g_list_model_items_changed (G_LIST_MODEL (self), 0, oldSize, g_list_model_get_n_items (G_LIST_MODEL (self->symbol_list)));
}

static void
yahoo_finance_search_model_search_response_cb (GObject *source, GAsyncResult *result, gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (JsonParser) parser = NULL;
  g_autoptr (GBytes) bytes = NULL;
  YahooFinanceSearchModel *self = user_data;

  bytes = soup_session_send_and_read_finish (SOUP_SESSION (source), result, &error);

  // Usage here is the same as before
  if (error)
    {
      g_warning ("Unable to receive YahooFinance response: %s", error->message);
      return;
    }

  parser = json_parser_new ();
  json_parser_load_from_data (parser, g_bytes_get_data (bytes, NULL), g_bytes_get_size (bytes), &error);
  if (error)
    {
      g_warning ("Unable to parse YahooFinance response: %s", error->message);
      return;
    }

  // remove all previous items
  GListStore *symbols = g_list_store_new (STOCKS_TYPE_SYMBOL);
  g_autoptr (JsonReader) reader = json_reader_new (json_parser_get_root (parser));

  json_reader_read_member (reader, "quotes");
  gint quotes_count = json_reader_count_elements (reader);
  StocksSymbol *symbol = NULL;
  for (gint i = 0; i < quotes_count; i++)
    {
      json_reader_read_element (reader, i);

      json_reader_read_member (reader, "symbol");
      symbol = stocks_symbol_new (json_reader_get_string_value (reader));
      json_reader_end_member (reader);

      if (json_reader_read_member (reader, "shortname"))
        stocks_symbol_set_shortname (symbol, json_reader_get_string_value (reader));
      json_reader_end_member (reader);

      if (json_reader_read_member (reader, "longname"))
        stocks_symbol_set_longname (symbol, json_reader_get_string_value (reader));
      json_reader_end_member (reader);

      if (json_reader_read_member (reader, "sector"))
        stocks_symbol_set_sector (symbol, json_reader_get_string_value (reader));
      json_reader_end_member (reader);

      if (json_reader_read_member (reader, "industry"))
        stocks_symbol_set_industry (symbol, json_reader_get_string_value (reader));
      json_reader_end_member (reader);

      json_reader_end_element (reader);

      g_list_store_append (symbols, symbol);
    }
  json_reader_end_member (reader);
  yahoo_finance_search_model_query_update_symbols (self, symbols);
}

static void
yahoo_finance_search_model_query_update_search (YahooFinanceSearchModel *self)
{
  g_autoptr (GUri) uri = NULL;
  g_autoptr (GString) query = NULL;

  g_cancellable_cancel (self->cancellable);

  if (self->search_text == NULL || g_utf8_strlen (self->search_text, -1) == 0)
    {
      guint oldSize = g_list_model_get_n_items (G_LIST_MODEL (self->symbol_list));
      g_list_store_remove_all (self->symbol_list);
      self->symbol_list = NULL;
      g_list_model_items_changed (G_LIST_MODEL (self), 0, oldSize, 0);
      return;
    }

  self->cancellable = g_cancellable_new ();

  if (self->search_text)
    {
      query = g_string_new ("newsCount=0&q={q}");
      g_string_replace (query, "{q}", self->search_text, 0);
    }

  uri = g_uri_build (G_URI_FLAGS_NONE, "https", NULL, YAHOO_HOST, -1, SEARCH_ROUTE, query->str, NULL);
  SoupMessage *msg = soup_message_new_from_uri (SOUP_METHOD_GET, uri);

  soup_session_send_and_read_async (
      self->session,
      msg,
      G_PRIORITY_DEFAULT,
      self->cancellable,
      yahoo_finance_search_model_search_response_cb,
      self);
}

void
yahoo_finance_search_model_query_update_symbols (YahooFinanceSearchModel *self, GListStore *symbols)
{
  if (symbols == NULL || g_list_model_get_n_items (G_LIST_MODEL (symbols)) == 0)
    return;

  yahoo_finance_symbol_query_update_symbols (self->symbol_query,
                                             symbols,
                                             self->cancellable,
                                             G_CALLBACK (yahoo_finance_search_model_update_cb),
                                             self);
}
