/* yahoo-finance-symbol-query.h
 *
 * Copyright 2023 Stephan Vedder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gio/gio.h>

#include "../stocks-symbol.h"

G_BEGIN_DECLS

#define YAHOO_FINANCE_TYPE_SYMBOL_QUERY (yahoo_finance_symbol_query_get_type ())

typedef void (*YahooFinanceSymbolQueryUpdateCb) (GListStore *symbols, gpointer user_data);

G_DECLARE_FINAL_TYPE (YahooFinanceSymbolQuery, yahoo_finance_symbol_query, YAHOO_FINANCE, SYMBOL_QUERY, GObject)
YahooFinanceSymbolQuery *yahoo_finance_symbol_query_new ();

void yahoo_finance_symbol_query_update_symbols (YahooFinanceSymbolQuery *self,
                                                GListStore *list,
                                                GCancellable *cancellable,
                                                GCallback callback,
                                                gpointer user_data);

G_END_DECLS
