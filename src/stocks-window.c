/* stocks-window.c
 *
 * Copyright 2023 Stephan Vedder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stocks-window.h"
#include "stocks-config.h"
#include "stocks-symbol-detail.h"
#include "stocks-symbol-info.h"
#include "stocks-symbol.h"
#include "yahoo-finance/yahoo-finance-search-model.h"

struct _StocksWindow
{
  AdwApplicationWindow parent_instance;

  YahooFinanceSearchModel *yahoo_search;

  /* Template widgets */
  GtkListBox *search_listbox;
  GtkListBox *stocks_listbox;
  AdwLeaflet *leaflet;
  AdwBin *detail_page;
};

G_DEFINE_TYPE (StocksWindow, stocks_window, ADW_TYPE_APPLICATION_WINDOW)

static void
leaflet_back_cb (StocksWindow *self)
{
  g_return_if_fail (STOCKS_IS_WINDOW (self));

  adw_bin_set_child (self->detail_page, NULL);
  adw_leaflet_navigate (self->leaflet, ADW_NAVIGATION_DIRECTION_BACK);
}

static void
leaflet_forward_cb (StocksWindow *self)
{
  g_return_if_fail (STOCKS_IS_WINDOW (self));

  adw_leaflet_navigate (self->leaflet, ADW_NAVIGATION_DIRECTION_FORWARD);
}

static void
listbox_row_activated (
    GtkListBox *self,
    GtkListBoxRow *row,
    gpointer user_data)
{
  StocksWindow *window = user_data;
  g_return_if_fail (STOCKS_IS_WINDOW (window));

  adw_bin_set_child (window->detail_page, NULL);
  StocksSymbol *symbol = g_list_model_get_item (G_LIST_MODEL (window->yahoo_search), gtk_list_box_row_get_index (row));
  adw_bin_set_child (window->detail_page, GTK_WIDGET (stocks_symbol_detail_new (symbol)));

  leaflet_forward_cb (window);
}

static void
stocks_window_class_init (StocksWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  g_type_ensure (YAHOO_FINANCE_TYPE_SEARCH_MODEL);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/feliwir/stocks/stocks-window.ui");
  gtk_widget_class_bind_template_child (widget_class, StocksWindow, search_listbox);
  gtk_widget_class_bind_template_child (widget_class, StocksWindow, stocks_listbox);
  gtk_widget_class_bind_template_child (widget_class, StocksWindow, leaflet);
  gtk_widget_class_bind_template_child (widget_class, StocksWindow, detail_page);
  gtk_widget_class_bind_template_child (widget_class, StocksWindow, yahoo_search);

  gtk_widget_class_bind_template_callback (widget_class, leaflet_back_cb);
  gtk_widget_class_bind_template_callback (widget_class, leaflet_forward_cb);
  gtk_widget_class_bind_template_callback (widget_class, listbox_row_activated);
}

static GtkWidget *
create_search_listbox_row (gpointer item,
                           gpointer user_data)
{
  StocksSymbol *symbol = (StocksSymbol *) item;
  GtkWidget *row;

  g_type_ensure (STOCKS_TYPE_SYMBOL_INFO);

  row = adw_action_row_new ();
  adw_preferences_row_set_title (ADW_PREFERENCES_ROW (row), stocks_symbol_get_symbol (symbol));
  adw_action_row_set_subtitle (ADW_ACTION_ROW (row), stocks_symbol_get_longname (symbol));
  adw_action_row_add_suffix (ADW_ACTION_ROW (row), GTK_WIDGET (stocks_symbol_info_new (symbol)));
  gtk_list_box_row_set_activatable (GTK_LIST_BOX_ROW (row), TRUE);

  return row;
}

static void
stocks_window_init (StocksWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  gtk_list_box_bind_model (self->search_listbox, G_LIST_MODEL (self->yahoo_search), create_search_listbox_row, NULL, NULL);
}
