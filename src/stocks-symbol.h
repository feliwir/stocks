/* stocks-symbol.h
 *
 * Copyright 2023 Stephan Vedder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "stocks-currency.h"
#include <gio/gio.h>

G_BEGIN_DECLS

#define STOCKS_TYPE_SYMBOL (stocks_symbol_get_type ())

G_DECLARE_FINAL_TYPE (StocksSymbol, stocks_symbol, STOCKS, SYMBOL, GObject)
StocksSymbol *stocks_symbol_new (const gchar *symbol);

const gchar *stocks_symbol_get_symbol (StocksSymbol *self);

void stocks_symbol_set_shortname (StocksSymbol *self, const gchar *shortname);
const gchar *stocks_symbol_get_shortname (StocksSymbol *self);

void stocks_symbol_set_longname (StocksSymbol *self, const gchar *longname);
const gchar *stocks_symbol_get_longname (StocksSymbol *self);

void stocks_symbol_set_sector (StocksSymbol *self, const gchar *sector);
const gchar *stocks_symbol_get_sector (StocksSymbol *self);

void stocks_symbol_set_industry (StocksSymbol *self, const gchar *industry);
const gchar *stocks_symbol_get_industry (StocksSymbol *self);

void stocks_symbol_set_price (StocksSymbol *self, const gdouble price);
gdouble stocks_symbol_get_price (StocksSymbol *self);

void stocks_symbol_set_market_change (StocksSymbol *self, const gdouble market_change);
gdouble stocks_symbol_get_market_change (StocksSymbol *self);

void stocks_symbol_set_currency (StocksSymbol *self, const StocksCurrency currency);
StocksCurrency stocks_symbol_get_currency (StocksSymbol *self);

G_END_DECLS
