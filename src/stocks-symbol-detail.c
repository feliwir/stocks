/* stocks-symbol-detail.c
 *
 * Copyright 2023 Stephan Vedder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stocks-symbol-detail.h"

struct _StocksSymbolDetail
{
  AdwBin parent_instance;

  StocksSymbol *symbol;
};

G_DEFINE_TYPE (StocksSymbolDetail, stocks_symbol_detail, ADW_TYPE_BIN)

enum
{
  PROP_0,
  PROP_SYMBOL,
  N_PROPS
};
static GParamSpec *properties[N_PROPS];

StocksSymbolDetail *
stocks_symbol_detail_new (StocksSymbol *symbol)
{
  return g_object_new (STOCKS_TYPE_SYMBOL_DETAIL,
                       "symbol", symbol,
                       NULL);
}

static void
stocks_symbol_detail_get_property (GObject *object,
                                   guint prop_id,
                                   GValue *value,
                                   GParamSpec *pspec)
{
  StocksSymbolDetail *self = STOCKS_SYMBOL_DETAIL (object);

  switch (prop_id)
    {
    case PROP_SYMBOL:
      g_value_set_object (value, self->symbol);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
stocks_symbol_detail_set_property (GObject *object,
                                   guint prop_id,
                                   const GValue *value,
                                   GParamSpec *pspec)
{
  StocksSymbolDetail *self = STOCKS_SYMBOL_DETAIL (object);

  switch (prop_id)
    {
    case PROP_SYMBOL:
      g_set_object (&self->symbol, g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
stocks_symbol_detail_class_init (StocksSymbolDetailClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = stocks_symbol_detail_get_property;
  object_class->set_property = stocks_symbol_detail_set_property;

  g_type_ensure (STOCKS_TYPE_SYMBOL);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/feliwir/stocks/stocks-symbol-detail.ui");
  gtk_widget_class_bind_template_child (widget_class, StocksSymbolDetail, symbol);

  /**
   * StocksSymbolDetail:symbol:
   *
   * The symbol we want to show details for
   */
  properties[PROP_SYMBOL] =
      g_param_spec_object ("symbol", NULL, NULL,
                           STOCKS_TYPE_SYMBOL,
                           G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
stocks_symbol_detail_init (StocksSymbolDetail *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
