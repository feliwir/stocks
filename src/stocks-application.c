/* stocks-application.c
 *
 * Copyright 2023 Stephan Vedder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stocks-application.h"
#include "stocks-window.h"

struct _StocksApplication
{
  AdwApplication parent_instance;
};

G_DEFINE_TYPE (StocksApplication, stocks_application, ADW_TYPE_APPLICATION)

StocksApplication *
stocks_application_new (gchar *application_id,
                        GApplicationFlags flags)
{
  g_return_val_if_fail (application_id != NULL, NULL);

  return g_object_new (STOCKS_TYPE_APPLICATION,
                       "application-id", application_id,
                       "flags", flags,
                       NULL);
}

static void
stocks_application_finalize (GObject *object)
{
  StocksApplication *self = (StocksApplication *) object;

  g_return_if_fail (STOCKS_IS_APPLICATION (self));

  G_OBJECT_CLASS (stocks_application_parent_class)->finalize (object);
}

static void
stocks_application_activate (GApplication *app)
{
  GtkWindow *window;

  g_assert (STOCKS_IS_APPLICATION (app));

  window = gtk_application_get_active_window (GTK_APPLICATION (app));
  if (window == NULL)
    window = g_object_new (STOCKS_TYPE_WINDOW,
                           "application", app,
                           NULL);

  gtk_window_present (window);
}

static void
stocks_application_class_init (StocksApplicationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->finalize = stocks_application_finalize;

  app_class->activate = stocks_application_activate;
}

static void
stocks_application_about_action (G_GNUC_UNUSED GSimpleAction *action,
                                 G_GNUC_UNUSED GVariant *parameter,
                                 gpointer user_data)
{
  static const gchar *developers[] = { "Stephan Vedder", NULL };
  StocksApplication *self = user_data;
  GtkWindow *window = NULL;

  g_return_if_fail (STOCKS_IS_APPLICATION (self));

  window = gtk_application_get_active_window (GTK_APPLICATION (self));

  adw_show_about_window (window, "application-name", "stocks", "application-icon",
                         "org.feliwir.stocks", "developer-name", "Stephan Vedder",
                         "version", "0.1.0", "developers", developers,
                         "copyright", "© 2023 Stephan Vedder", NULL);
}

static void
stocks_application_quit_action (G_GNUC_UNUSED GSimpleAction *action,
                                G_GNUC_UNUSED GVariant *parameter,
                                gpointer user_data)
{
  StocksApplication *self = user_data;

  g_assert (STOCKS_IS_APPLICATION (self));

  g_application_quit (G_APPLICATION (self));
}

static const GActionEntry app_actions[] = {
  { "quit", stocks_application_quit_action },
  { "about", stocks_application_about_action },
};

static void
stocks_application_init (StocksApplication *self)
{
  g_action_map_add_action_entries (G_ACTION_MAP (self), app_actions,
                                   G_N_ELEMENTS (app_actions), self);

  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "app.quit",
                                         (const char *[]){
                                             "<primary>q",
                                             NULL,
                                         });
}
