/* stocks-symbol.c
 *
 * Copyright 2023 Stephan Vedder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "stocks-symbol.h"
#include "stocks.h"

struct _StocksSymbol
{
  GObject parent_instance;

  gchar *symbol;
  gchar *shortname;
  gchar *longname;
  gchar *sector;
  gchar *industry;
  gdouble price;
  gdouble market_change;
  StocksCurrency currency;
};

G_DEFINE_TYPE (StocksSymbol, stocks_symbol, G_TYPE_OBJECT)

enum
{
  PROP_0,
  PROP_SYMBOL,
  PROP_SHORTNAME,
  PROP_LONGNAME,
  PROP_SECTOR,
  PROP_INDUSTRY,
  PROP_PRICE,
  PROP_MARKET_CHANGE,
  PROP_CURRENCY,
  N_PROPS
};
static GParamSpec *properties[N_PROPS];

StocksSymbol *
stocks_symbol_new (const gchar *symbol)
{
  return g_object_new (STOCKS_TYPE_SYMBOL,
                       "symbol", symbol,
                       NULL);
}

const gchar *
stocks_symbol_get_symbol (StocksSymbol *self)
{
  g_return_val_if_fail (STOCKS_IS_SYMBOL (self), NULL);

  return self->symbol;
}

void
stocks_symbol_set_shortname (StocksSymbol *self, const gchar *shortname)
{
  g_return_if_fail (STOCKS_IS_SYMBOL (self));

  if (g_strcmp0 (self->shortname, shortname) != 0)
    {
      g_free (self->shortname);
      self->shortname = g_strdup (shortname);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SHORTNAME]);
    }
}

const gchar *
stocks_symbol_get_shortname (StocksSymbol *self)
{
  g_return_val_if_fail (STOCKS_IS_SYMBOL (self), NULL);

  return self->shortname;
}

void
stocks_symbol_set_longname (StocksSymbol *self, const gchar *longname)
{
  g_return_if_fail (STOCKS_IS_SYMBOL (self));

  if (g_strcmp0 (self->shortname, longname) != 0)
    {
      g_free (self->longname);
      self->longname = g_strdup (longname);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_LONGNAME]);
    }
}

const gchar *
stocks_symbol_get_longname (StocksSymbol *self)
{
  g_return_val_if_fail (STOCKS_IS_SYMBOL (self), NULL);

  return self->longname;
}

void
stocks_symbol_set_sector (StocksSymbol *self, const gchar *sector)
{
  g_return_if_fail (STOCKS_IS_SYMBOL (self));

  if (g_strcmp0 (self->sector, sector) != 0)
    {
      g_free (self->sector);
      self->sector = g_strdup (sector);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SECTOR]);
    }
}

const gchar *
stocks_symbol_get_sector (StocksSymbol *self)
{
  g_return_val_if_fail (STOCKS_IS_SYMBOL (self), NULL);

  return self->sector;
}

void
stocks_symbol_set_industry (StocksSymbol *self, const gchar *industry)
{
  g_return_if_fail (STOCKS_IS_SYMBOL (self));

  if (g_strcmp0 (self->industry, industry) != 0)
    {
      g_free (self->industry);
      self->industry = g_strdup (industry);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_INDUSTRY]);
    }
}

const gchar *
stocks_symbol_get_industry (StocksSymbol *self)
{
  g_return_val_if_fail (STOCKS_IS_SYMBOL (self), NULL);

  return self->industry;
}

void
stocks_symbol_set_price (StocksSymbol *self, const gdouble price)
{
  g_return_if_fail (STOCKS_IS_SYMBOL (self));

  self->price = price;

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_PRICE]);
}

gdouble
stocks_symbol_get_price (StocksSymbol *self)
{
  g_return_val_if_fail (STOCKS_IS_SYMBOL (self), 0.0f);

  return self->price;
}

void
stocks_symbol_set_market_change (StocksSymbol *self, const gdouble market_change)
{
  g_return_if_fail (STOCKS_IS_SYMBOL (self));

  self->market_change = market_change;

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_MARKET_CHANGE]);
}

gdouble
stocks_symbol_get_market_change (StocksSymbol *self)
{
  g_return_val_if_fail (STOCKS_IS_SYMBOL (self), 0.0f);

  return self->market_change;
}

void
stocks_symbol_set_currency (StocksSymbol *self, const StocksCurrency currency)
{
  g_return_if_fail (STOCKS_IS_SYMBOL (self));

  self->currency = currency;

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_CURRENCY]);
}

StocksCurrency
stocks_symbol_get_currency (StocksSymbol *self)
{
  g_return_val_if_fail (STOCKS_IS_SYMBOL (self), STOCKS_CURRENCY_USD);

  return self->currency;
}

static void
stocks_symbol_get_property (GObject *object,
                            guint prop_id,
                            GValue *value,
                            GParamSpec *pspec)
{
  StocksSymbol *self = STOCKS_SYMBOL (object);

  switch (prop_id)
    {
    case PROP_SYMBOL:
      g_value_set_string (value, self->symbol);
      break;
    case PROP_SHORTNAME:
      g_value_set_string (value, self->shortname);
      break;
    case PROP_LONGNAME:
      g_value_set_string (value, self->longname);
      break;
    case PROP_SECTOR:
      g_value_set_string (value, self->sector);
      break;
    case PROP_INDUSTRY:
      g_value_set_string (value, self->industry);
      break;
    case PROP_PRICE:
      g_value_set_double (value, self->price);
      break;
    case PROP_MARKET_CHANGE:
      g_value_set_double (value, self->market_change);
      break;
    case PROP_CURRENCY:
      g_value_set_enum (value, self->currency);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
stocks_symbol_set_property (GObject *object,
                            guint prop_id,
                            const GValue *value,
                            GParamSpec *pspec)
{
  StocksSymbol *self = STOCKS_SYMBOL (object);

  switch (prop_id)
    {
    case PROP_SYMBOL:
      self->symbol = g_strdup (g_value_get_string (value));
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SYMBOL]);
      break;
    case PROP_SHORTNAME:
      stocks_symbol_set_shortname (self, g_value_get_string (value));
      break;
    case PROP_LONGNAME:
      stocks_symbol_set_longname (self, g_value_get_string (value));
      break;
    case PROP_SECTOR:
      stocks_symbol_set_sector (self, g_value_get_string (value));
      break;
    case PROP_INDUSTRY:
      stocks_symbol_set_industry (self, g_value_get_string (value));
      break;
    case PROP_PRICE:
      {
        double price = g_value_get_double (value);
        stocks_symbol_set_price (self, price);
        break;
      }
    case PROP_MARKET_CHANGE:
      stocks_symbol_set_market_change (self, g_value_get_double (value));
      break;
    case PROP_CURRENCY:
      stocks_symbol_set_currency (self, g_value_get_enum (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
stocks_symbol_class_init (StocksSymbolClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = stocks_symbol_get_property;
  object_class->set_property = stocks_symbol_set_property;

  /**
   * StocksSymbol:symbol:
   *
   * The symbol name of the current stock
   */
  properties[PROP_SYMBOL] =
      g_param_spec_string ("symbol", NULL, NULL,
                           NULL,
                           G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

  /**
   * StocksSymbol:shortname:
   *
   * The shortname name of the current stock
   */
  properties[PROP_SHORTNAME] =
      g_param_spec_string ("shortname", NULL, NULL,
                           NULL,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

  /**
   * StocksSymbol:longname:
   *
   * The longname name of the current stock
   */
  properties[PROP_LONGNAME] =
      g_param_spec_string ("longname", NULL, NULL,
                           NULL,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

  /**
   * StocksSymbol:sector:
   *
   * The sector the current stock operates in
   */
  properties[PROP_SECTOR] =
      g_param_spec_string ("sector", NULL, NULL,
                           NULL,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

  /** StocksSymbol:industry:
   *
   * The industry the current stock operates in
   */
  properties[PROP_INDUSTRY] =
      g_param_spec_string ("industry", NULL, NULL,
                           NULL,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

  /** StocksSymbol:price:
   *
   * The price of the stock
   */
  properties[PROP_PRICE] =
      g_param_spec_double ("price", NULL, NULL,
                           0.0f, G_MAXFLOAT, 0.0f,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

  /** StocksSymbol:market-change:
   *
   * The market change of the stock
   */
  properties[PROP_MARKET_CHANGE] =
      g_param_spec_double ("market-change", NULL, NULL,
                           -G_MAXDOUBLE, G_MAXDOUBLE, 0.0f,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

  /** StocksSymbol:currency:
   *
   * The currency of the stock
   */
  properties[PROP_CURRENCY] =
      g_param_spec_enum ("currency", NULL, NULL,
                         STOCKS_TYPE_CURRENCY, STOCKS_CURRENCY_USD,
                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
stocks_symbol_init (StocksSymbol *self)
{
}
